#!/bin/bash
# https://github.com/rocker-org/rocker-versioned2/blob/master/scripts/patch_install_command.sh
set -e

ln -sf /rocker_scripts/bin/install2.r  /usr/local/bin/install2.r
